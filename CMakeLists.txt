cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME droneOpenTLDTranslatorROS)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithDebInfo)



#DRONE_OPEN_TLD_INTERFACE_ROS 
set(DRONE_OPEN_TLD_INTERFACE_ROS_SOURCE_DIR
	src/sources) 
	
set(DRONE_OPEN_TLD_INTERFACE_ROS_INCLUDE_DIR
	src/include)

set(DRONE_OPEN_TLD_INTERFACE_ROS_SOURCE_FILES
        ${DRONE_OPEN_TLD_INTERFACE_ROS_SOURCE_DIR}/OpenTLDTranslator.cpp
       )

set(DRONE_OPEN_TLD_INTERFACE_ROS_HEADER_FILES
        ${DRONE_OPEN_TLD_INTERFACE_ROS_INCLUDE_DIR}/OpenTLDTranslator.h
       )

find_package(catkin REQUIRED roscpp std_msgs droneMsgsROS droneModuleROS tld_msgs cv_bridge sensor_msgs)

find_package(OpenCV REQUIRED)

catkin_package(
        INCLUDE_DIRS ${DRONE_OPEN_TLD_INTERFACE_ROS_INCLUDE_DIR}
        LIBRARIES droneOpenTLDTranslator
        CATKIN_DEPENDS roscpp std_msgs droneMsgsROS droneModuleROS tld_msgs cv_bridge sensor_msgs
        DEPENDS OpenCV
      )


include_directories(${DRONE_OPEN_TLD_INTERFACE_ROS_INCLUDE_DIR})
include_directories(${catkin_INCLUDE_DIRS})
include_directories(${OpenCV_INCLUDE_DIRS})

add_library(droneOpenTLDTranslator ${DRONE_OPEN_TLD_INTERFACE_ROS_SOURCE_FILES} ${DRONE_OPEN_TLD_INTERFACE_ROS_HEADER_FILES})
add_dependencies(droneOpenTLDTranslator ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneOpenTLDTranslator ${catkin_LIBRARIES})
target_link_libraries(droneOpenTLDTranslator ${OpenCV_LIBS})

 
#common commands for building c++ executables and libraries


add_executable(droneOpenTLDTranslatorROSNode ${DRONE_OPEN_TLD_INTERFACE_ROS_SOURCE_DIR}/OpenTLDTranslatorNode.cpp)
add_dependencies(droneOpenTLDTranslatorROSNode ${catkin_EXPORTED_TARGETS})
target_link_libraries(droneOpenTLDTranslatorROSNode ${catkin_LIBRARIES})
target_link_libraries(droneOpenTLDTranslatorROSNode droneOpenTLDTranslator)
target_link_libraries(droneOpenTLDTranslatorROSNode ${OpenCV_LIBS})


