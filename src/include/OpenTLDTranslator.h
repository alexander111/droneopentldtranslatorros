#include "ros/ros.h"

#include  "droneMsgsROS/BoundingBox.h"
#include  "droneMsgsROS/Target.h"
#include  "droneMsgsROS/VectorROIs.h"
#include  "cv_bridge/cv_bridge.h"
#include  "sensor_msgs/Image.h"
#include  "sensor_msgs/image_encodings.h"

#include "std_msgs/Char.h"
#include "tld_msgs/BoundingBox.h"
#include "tld_msgs/Target.h"

#include "std_msgs/Float32.h"

#include "droneModuleROS.h"


class OpenTLDTranslator : public DroneModule
{

public:
     OpenTLDTranslator();
     ~OpenTLDTranslator();

public:
     void init();
     void close();

protected:
     bool resetValues();
     bool startVal();
     bool stopVal();

public:
    bool run();

public:
     void open(ros::NodeHandle & nIn);

public: //topic names

     //Subscribers
     std::string tld_tracked_object_topic_name;
     std::string tld_fps_topic_name;
     std::string ROI_topic_name;
     std::string tld_commands_topic_name;

     //Publishers
     std::string Open_tld_translator_tracked_object_topic_name;
     std::string Open_tld_translator_fps_topic_name;
     std::string tld_gui_bb_topic_name;
     std::string tld_gui_cmds_topic_name;

private :


    ros::Subscriber bounding_box_tld_sub;
    ros::Subscriber fps_tld_sub;
    ros::Subscriber ROI_sub;
    ros::Subscriber tld_command_sub;
    void boundingboxCallback(const tld_msgs::BoundingBox::ConstPtr& msg);
    void fpsCallback(const std_msgs::Float32::ConstPtr& msg);
    void ROICallback(const droneMsgsROS::VectorROIs::ConstPtr& msg);
    void tldcommandsubCallback(const std_msgs::Char::ConstPtr& msg);

    ros::Publisher bounding_box_traker_eye_pub;
    ros::Publisher fps_traker_eye_pub;
    ros::Publisher ROI_pub;
    ros::Publisher tld_command_pub;

    sensor_msgs::Image Image;
    tld_msgs::Target ROI;
    droneMsgsROS::BoundingBox bounding_box;
    std_msgs::Float32 fps;
    std_msgs::Char cmd;
};
