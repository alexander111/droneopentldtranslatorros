#include "OpenTLDTranslator.h"

OpenTLDTranslator::OpenTLDTranslator():
    DroneModule(droneModule::active, 1.0/(1.0/15.0))
{
    std::cout << "OpenTLDTranslator(..), enter and exit " << std::endl;
    return;
}

OpenTLDTranslator::~OpenTLDTranslator()
{
    close();
    return;
}

void OpenTLDTranslator::init()
{
    return;
}


void OpenTLDTranslator::close()
{
    return;
}

bool OpenTLDTranslator::resetValues()
{
    if(!DroneModule::resetValues())
        return false;

    cmd.data = 'c';
    tld_command_pub.publish(cmd);
    std::cout << "Clearing and stop tracking" << std::endl;
    return true;
}

bool OpenTLDTranslator::startVal()
{


    if(!DroneModule::startVal())
        return false;

    return true;

}

bool OpenTLDTranslator::stopVal()
{
    if(!DroneModule::stopVal())
        return false;

    cmd.data = 'c';
    tld_command_pub.publish(cmd);


    bounding_box.x   = 0;
    bounding_box.y   = 0;
    bounding_box.width = 0;
    bounding_box.height = 0;
    bounding_box.confidence = 0;

    bounding_box_traker_eye_pub.publish(bounding_box);

    std::cout << "Clearing and stop tracking" << std::endl;
    return true;
}

bool OpenTLDTranslator::run()
{
    if(!DroneModule::run())
        return false;

    return true;
}


void OpenTLDTranslator::open(ros::NodeHandle &nIn)
{
    DroneModule::open(nIn);

    //Subscriber names
    ros::param::get("~tld_tracked_object_topic_name", tld_tracked_object_topic_name);
    if( tld_tracked_object_topic_name.length() == 0)
    {
        tld_tracked_object_topic_name="tld_tracked_object";
    }
    ros::param::get("~tld_fps_topic_name", tld_fps_topic_name);
    if( tld_fps_topic_name.length() == 0)
    {
        tld_fps_topic_name="tld_fps";
    }
    ros::param::get("~ROI_topic_name", ROI_topic_name);
    if( ROI_topic_name.length() == 0 )
    {
        ROI_topic_name="ROI";
    }
    ros::param::get("~tld_commands_topic_name", tld_commands_topic_name);
    if( tld_commands_topic_name.length() == 0)
    {
        tld_commands_topic_name="tld_commands";
    }

       //Publisher names
    ros::param::get("~Open_tld_translator_tracked_object_topic_name", Open_tld_translator_tracked_object_topic_name);
    if( Open_tld_translator_tracked_object_topic_name.length() == 0)
    {
        Open_tld_translator_tracked_object_topic_name="Open_tld_translator/tracked_object";
    }
    ros::param::get("~Open_tld_translator_fps_topic_name", Open_tld_translator_fps_topic_name);
    if( Open_tld_translator_fps_topic_name.length() == 0)
    {
        Open_tld_translator_fps_topic_name="Open_tld_translator/fps";
    }
    ros::param::get("~tld_gui_bb_topic_name", tld_gui_bb_topic_name);
    if( tld_gui_bb_topic_name.length() == 0)
    {
        tld_gui_bb_topic_name="tld_gui_bb";
    }

    ros::param::get("~tld_gui_cmds_topic_name", tld_gui_cmds_topic_name);
    if( tld_gui_cmds_topic_name.length() == 0)
    {
        tld_gui_cmds_topic_name="tld_gui_cmds";
    }

    bounding_box_tld_sub = n.subscribe(tld_tracked_object_topic_name, 1, &OpenTLDTranslator::boundingboxCallback,this);
    fps_tld_sub          = n.subscribe(tld_fps_topic_name,1, &OpenTLDTranslator::fpsCallback,this);
    ROI_sub              = n.subscribe(ROI_topic_name,1,&OpenTLDTranslator::ROICallback,this);
    tld_command_sub      = n.subscribe(tld_commands_topic_name,1,&OpenTLDTranslator::tldcommandsubCallback, this);

    bounding_box_traker_eye_pub = n.advertise<droneMsgsROS::BoundingBox>(Open_tld_translator_tracked_object_topic_name, 1 , true);
    fps_traker_eye_pub          = n.advertise<std_msgs::Float32>(Open_tld_translator_fps_topic_name,1, true);
    ROI_pub                     = n.advertise<tld_msgs::Target>(tld_gui_bb_topic_name,1,true);
    tld_command_pub             = n.advertise<std_msgs::Char>(tld_gui_cmds_topic_name,1,true);

    droneModuleOpened = true;

    //Auto-Start module
    //moduleStarted=true;

    return;
}

void OpenTLDTranslator::boundingboxCallback(const tld_msgs::BoundingBox::ConstPtr& msg)
{

    bounding_box.x   = msg->x;
    bounding_box.y   = msg->y;
    bounding_box.width = msg->width;
    bounding_box.height = msg->height;
    bounding_box.confidence = msg->confidence;

    if(!run())
        return;

    bounding_box_traker_eye_pub.publish(bounding_box);

}

void OpenTLDTranslator::fpsCallback(const std_msgs::Float32::ConstPtr& msg)
{
    fps.data = msg->data;

    if(!run())
        return;

    fps_traker_eye_pub.publish(fps);

}

void OpenTLDTranslator::ROICallback(const droneMsgsROS::VectorROIs::ConstPtr& msg)
{
    if(!run())
        return;

    for(int i=0; i <msg->obs.size(); i++)
    {
        ROI.bb.x = msg->obs[i].bb.x;
        ROI.bb.y = msg->obs[i].bb.y;
        ROI.bb.width = msg->obs[i].bb.width;
        ROI.bb.height = msg->obs[i].bb.height;
        ROI.bb.confidence = msg->obs[i].bb.confidence;
        ROI.img = msg->obs[i].img;
        ROI_pub.publish(ROI);
    }

}

void OpenTLDTranslator::tldcommandsubCallback(const std_msgs::Char::ConstPtr& msg)
{
    if(!run())
        return;

    switch(msg->data)
    {
    case 'b':
    cmd.data = 'b';
    tld_command_pub.publish(cmd);
    std::cout << "Clearing Background" << std::endl;
    break;

    case 'c':
    cmd.data = 'c';
    tld_command_pub.publish(cmd);
    std::cout << "Clearing and stop tracking" << std::endl;
    break;

    case 'l':
    cmd.data = 'l';
    tld_command_pub.publish(cmd);
    std::cout << "Toggle learning" << std::endl;
    break;

    case 'a':
    cmd.data = 'a';
    tld_command_pub.publish(cmd);
    std::cout << "Alternating mode" << std::endl;
    break;

    case 'r':
    cmd.data = 'r';
    tld_command_pub.publish(cmd);
    std::cout << "Reset" << std::endl;
    break;
    }
}
