#include "ros/ros.h"
#include "OpenTLDTranslator.h"
#include "nodes_definition.h"


int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, MODUlE_NAME_OPENTLD_TRANSLATOR);
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << ros::this_node::getName() << std::endl;

    OpenTLDTranslator MyOpenTLDTranslator;
    MyOpenTLDTranslator.open(n);

    try
    {
        ros::spin();
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }
    return 0;
}
